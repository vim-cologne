" Specify initial buffer location by filename suffix.
" Author: Evan Hanson <evhan@foldling.org>
" Version: 0.0.5
" Homepage: https://git.foldling.org/vim-cologne
" Repository: https://git.foldling.org/vim-cologne.git
" Last Change: 2019-06-03
" License: BSD

if exists('g:cologne_loaded') || &compatible || version < 700
  finish
else
  let g:cologne_loaded = 1
endif

function! s:Reopen(parts)
  let [filename; _] = a:parts
  silent execute "edit " . fnameescape(filename)
endfunction

function! s:ReopenMotion(parts)
  let [filename, motion] = a:parts

  if bufexists(filename) == 0
    silent execute "argedit " . fnameescape(filename)
  else
    silent execute "edit " . fnameescape(filename)
  end

  if motion =~ '^\d\+$'
    silent! execute "normal! " . motion . "gg"
  elseif motion =~ '^\d*[/?].*$'
    silent! execute "normal! " . motion . "\<cr>"
  elseif motion =~ '^\s*$'
    silent! normal! 0go
  else
    " NOTE The use of normal (as opposed to :normal!) is intentional.
    silent! execute "normal " . motion
  end
endfunction

function! s:ProcessFile()
  let filename = expand('%')
  let parts = split(filename, ':', 1)
  " Preserve protocol filenames for Netrw (e.g. "foo://").
  if filename =~? '^[a-z]\+://'
    return
  elseif len(parts) >= 2
    try
      call s:ReopenMotion(parts[0:1])
      execute "bwipeout! " . fnameescape(filename)
      execute "argdelete " . fnameescape(filename)
    catch
      echohl WarningMsg
      echo "Invalid motion: " . parts[1]
      echohl None
    endtry
  elseif filereadable(filename)
    call s:Reopen(parts)
  end
endfunction

function! s:ProcessArguments()
  if argc()
    argdo! call s:ProcessFile()
  endif
  doautoall Syntax
endfunction

augroup cologne
  autocmd!
  autocmd VimEnter   * nested call s:ProcessArguments()
  autocmd BufNewFile * nested call s:ProcessFile()
augroup END
