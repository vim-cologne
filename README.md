vim-cologne
===========
Specify an initial buffer position by filename suffix.

Usage
-----
This plugin allows one to open files with the initial cursor position
set by a colon-separated filename suffix:

    vim foo.txt:10   # line 10
    vim foo.txt:/foo # first "foo"

The same applies for any buffers opened with `:edit`, `:args`, and so
on. The suffix can be any motion:

    vim foo.txt:10w    # tenth word
    vim foo.txt:3}     # third paragraph
    vim foo.txt:8/foo  # eighth foo

History
-------
 * 0.0.5 - Silence spurious messages from new buffers.
 * 0.0.4 - Handle invalid motions more gracefully.
 * 0.0.3 - Leave Netrw filenames unchanged.
 * 0.0.2 - Add support for filenames with spaces.
 * 0.0.1 - Initial release.

Licence
------
Copyright © 2015-2019 Evan Hanson <evhan@foldling.org>

BSD-style license. See `LICENSE` for details.
